# coding: utf-8

import asyncio
import logging

import aioredis

from requestscounter.utils import settings


class async_run_once:

    class EmptyCache:
        pass

    EMPTY_CACHE = EmptyCache()

    def __init__(self, async_func):
        self.async_func = async_func
        self.cache = self.EMPTY_CACHE

    async def __call__(self, *args, **kwargs):
        if self.cache is self.EMPTY_CACHE:
            self.cache = await self.async_func(*args, **kwargs)
        return self.cache


@async_run_once
async def get_redis_client() -> aioredis.Redis:
    address = (settings.get('redis_host'), settings.get('redis_port'))
    redis = await aioredis.create_redis(address, loop=asyncio.get_event_loop())
    logging.info(f'Connected to Redis {address}')
    return redis


async def close_redis_client():
    redis = await get_redis_client()
    redis.close()
    await redis.wait_closed()
