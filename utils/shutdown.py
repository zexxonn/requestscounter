#!/usr/bin/env python
# coding: utf-8


import logging
import signal
import sys
from tornado.ioloop import IOLoop

_SHUTDOWN_HANDLERS = []


def setup_signals_handler():
    """ Установить обработчик сигналов.
    """
    signal.signal(signal.SIGTERM, _handle_shutdown)
    signal.signal(signal.SIGINT, _handle_shutdown)


def add_shutdown_handler(func, args=None, kwargs=None):
    args = args or tuple()
    kwargs = kwargs or dict()
    _SHUTDOWN_HANDLERS.append((func, args, kwargs))
    logging.info(
        'Registered new shutdown handler: func={}, args={}, kwargs={}',
        func, args, kwargs
    )


def _handle_shutdown(signum, _):
    logging.info('Starting global shutdown procedure: signal={}', signum)

    if not _SHUTDOWN_HANDLERS:
        logging.info('No shutdown handlers. Exiting')
        sys.exit()

    io_loop = IOLoop.current()
    for func, args, kwargs in _SHUTDOWN_HANDLERS:
        io_loop.add_callback_from_signal(func, *args, **kwargs)

    logging.info('All handlers are done')
