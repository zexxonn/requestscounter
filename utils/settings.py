from argparse import Namespace

SETTINGS_CACHE = dict()


def get(key, default=None):
    return SETTINGS_CACHE.get(key, default)


def init(args: Namespace):
    for key, value in vars(args).items():
        SETTINGS_CACHE[key] = value
