import asyncio
from functools import partial

import tornado.platform.asyncio as tornado_asyncio
import uvloop
from tornado import web
from tornado.ioloop import IOLoop

from requestscounter.handlers import IncrementCounterHandler, ManageCounterHandler
from requestscounter.utils import settings
from requestscounter.utils.service import get_redis_client, close_redis_client
from requestscounter.utils.shutdown import setup_signals_handler, add_shutdown_handler


class RequestCounterApp:

    @classmethod
    async def run(cls, port: int):
        web_app = web.Application([
            ('/count', ManageCounterHandler),
            ('/', IncrementCounterHandler)
        ])
        web_app.redis_client = await get_redis_client()
        web_app.counter_name = settings.get('counter_name')
        web_app.listen(port=port)


def run():
    asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())
    tornado_asyncio.AsyncIOMainLoop().install()

    app_runner = partial(RequestCounterApp.run, settings.get('port'))
    event_loop = IOLoop.current()
    event_loop.add_callback(app_runner)

    setup_signals_handler()
    add_shutdown_handler(close_redis_client)
    add_shutdown_handler(event_loop.stop)

    event_loop.start()
