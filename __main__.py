#!/usr/bin/env python3.6
# coding: utf-8


import argparse

from requestscounter.utils import settings


def main():
    parser = argparse.ArgumentParser(description='MicroService for requests counting')
    parser.add_argument(
        '-p', '--port', type=int, required=True, help=''
    )
    parser.add_argument(
        '--redis_port', type=int, required=False, default=6379, help=''
    )
    parser.add_argument(
        '--redis_host', type=str, required=False, default='127.0.0.1', help=''
    )
    parser.add_argument(
        '--counter_name', type=str, required=False, default='requests_total_count', help=''
    )
    args = parser.parse_args()
    settings.init(args)

    from . import server
    server.run()


if __name__ == '__main__':
    main()
