import ujson

from tornado import web


class ManageCounterHandler(web.RequestHandler):

    INITIAL_COUNTER_STATE = 0

    async def get(self):
        counter = await self.application.redis_client.get(
            key=self.application.counter_name,
            encoding='utf-8'
        )
        self.write(
            ujson.dumps(
                {'success': True, 'counter': counter or self.INITIAL_COUNTER_STATE}
            )
        )

    async def delete(self):
        await self.application.redis_client.set(
            key=self.application.counter_name,
            value=self.INITIAL_COUNTER_STATE
        )
        self.write(
            ujson.dumps({'success': True})
        )


class IncrementCounterHandler(web.RequestHandler):
    async def get(self):
        try:
            if self.request.body:
                ujson.loads(self.request.body)
            await self.application.redis_client.incr(
                key=self.application.counter_name
            )
        except Exception as ex:
            self.write(
                ujson.dumps({'success': False, 'result': str(ex)})
            )
        else:
            self.write(
                ujson.dumps({'success': True})
            )
